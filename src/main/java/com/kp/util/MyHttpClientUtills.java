package com.kp.util;

import java.io.IOException;

import javax.sound.midi.Soundbank;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

public class MyHttpClientUtills {

	/**
	 * 跨域请求工具
	 */

	public static String getNoParam(String url) {
		// xx获得Http客户端---》构建器
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		// xx创建Get请求
		HttpGet httpGet = new HttpGet(url);
		// xx响应模型
		CloseableHttpResponse response = null;
		try {
			// xx由客户端执行(发送)Get请求
			response = httpClient.execute(httpGet);
			// xx从响应模型中获取响应实体
			HttpEntity responseEntity = response.getEntity();
			System.out.println("响应状态为:" + response.getStatusLine());
			if (responseEntity != null) {
				System.out.println("响应内容长度为:" + responseEntity.getContentLength());
				// xx接口数据
				System.out.println("响应内容为:--->" + EntityUtils.toString(responseEntity));
				String json = EntityUtils.toString(responseEntity);
				if (json != null && !json.equals("")) {
					return json;
				} else {
					return "未获得任何http协议响应信息，请联系三方服务商";
				}
			} else {
				return "HttpStatus: 404";
			}
		} catch (ClientProtocolException e) {
			return "HttpStatus: 405";
		} catch (IOException e) {
			return "HttpStatus: 500";
		} finally {
			try {
				// x释放资源
				if (httpClient != null) {
					httpClient.close();
				}
				if (response != null) {
					response.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static String getHaveParam(String url, String param) {
		// x获得Http客户端
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		// x创建Get请求
		HttpGet httpGet = new HttpGet(url + param);
		// x响应模型
		CloseableHttpResponse response = null;
		try {
			// x配置信息
			RequestConfig requestConfig = RequestConfig.custom()
					// x设置连接超时时间(单位毫秒)
					.setConnectTimeout(5000)
					// x设置请求超时时间(单位毫秒)
					.setConnectionRequestTimeout(5000)
					// xsocket读写超时时间(单位毫秒)
					.setSocketTimeout(5000)
					// x设置是否允许重定向(默认为true)
					.setRedirectsEnabled(true).build();
			// x将上面的配置信息 运用到这个Get请求里
			httpGet.setConfig(requestConfig);

			// x由客户端执行(发送)Get请求
			response = httpClient.execute(httpGet);
			// x从响应模型中获取响应实体
			HttpEntity responseEntity = response.getEntity();
			if (responseEntity != null) {
				String json = EntityUtils.toString(responseEntity);
				if (json != null && !json.equals("")) {
					return json;
				} else {
					return "404";
				}
			} else {

				return "302";
			}
		} catch (Exception e) {
			return "500";
		} finally {
			try {
				// x释放资源
				if (httpClient != null) {
					httpClient.close();
				}
				if (response != null) {
					response.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

}
