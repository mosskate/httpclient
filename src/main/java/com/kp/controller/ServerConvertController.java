package com.kp.controller;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kp.pojo.MessageConvert;
import com.kp.pojo.ParamGetList;

@Controller
@RequestMapping("/server/")
public class ServerConvertController {
//测试开发
	   @GetMapping("init")
	   @ResponseBody
	   public    MessageConvert     initServer() throws Exception {
		   Class<MessageConvert> convert = (Class<MessageConvert>) Class.forName("com.kp.pojo.MessageConvert");
		   MessageConvert msg = convert.newInstance();
		   msg.setMsgConvertPrimarykey(6080);
		   msg.setMessageConvertTimes(new  Date());
		   msg.setEncodeTypeClient("utf-8");
		   msg.setEncodeTypeServer("ISO-8859-1");
		   msg.setConvertType(String.class);
   		   return  msg;
	   }
	   
	   @GetMapping("param")
	   @ResponseBody 
	   public    ParamGetList    paramValue(@RequestParam(value="paramReset",required=false)String paramReset,
			                                @RequestParam(value="paramNumber",required=false)Integer  paramNumber,
			                                @RequestParam(value="routingCount",required=false)String routingCount) throws InstantiationException, IllegalAccessException, Exception {
		   Class<ParamGetList> paramGet = (Class<ParamGetList>) Class.forName("com.kp.pojo.ParamGetList");
		   ParamGetList param = paramGet.newInstance();
	       param.setParamNumber(paramNumber);
	       if(routingCount!=null&&!"".equals(routingCount)) {
		       param.setRoutingCount(new BigDecimal(routingCount));
	       }
	       param.setParamReset(paramReset);
	       return  param;
	   }	   
	   
	   @PostMapping("postInit")
	   @ResponseBody
	   public    MessageConvert     initServerDoPost() throws Exception {
		   Class<MessageConvert> convert = (Class<MessageConvert>) Class.forName("com.kp.pojo.MessageConvert");
		   MessageConvert msg = convert.newInstance();
		   msg.setMsgConvertPrimarykey(6080);
		   msg.setMessageConvertTimes(new  Date());
		   msg.setEncodeTypeClient("utf-8");
		   msg.setEncodeTypeServer("ISO-8859-1");
		   msg.setConvertType(String.class);
   		   return  msg;
	   }
	   
	   @PostMapping("postParam")
	   @ResponseBody 
	   public    ParamGetList    paramValueDoPost(@RequestBody  ParamGetList paramGetList) throws InstantiationException, IllegalAccessException, Exception {
		   Class<ParamGetList> paramGet = (Class<ParamGetList>) Class.forName("com.kp.pojo.ParamGetList");
		   ParamGetList param = paramGet.newInstance();
	       param.setParamNumber(paramGetList.getParamNumber());
	       if(paramGetList.getRoutingCount()!=null&&!"".equals(paramGetList.getRoutingCount())) {
		       param.setRoutingCount(paramGetList.getRoutingCount());
	       }
	       param.setParamReset(paramGetList.getParamReset());
	       return  param;
	   }	   
}
