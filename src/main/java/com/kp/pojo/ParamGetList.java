package com.kp.pojo;

import java.math.BigDecimal;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class ParamGetList {

	
	  private   String         paramReset;
	  
	  private   Integer        paramNumber;
	  
	  private   BigDecimal     routingCount;

	  @Override
	  public String toString() {
		  return "ParamGetList [paramReset=" + paramReset + ", paramNumber=" + paramNumber + ", routingCount="
			    	+ routingCount + "]";
	  }
	  
}
