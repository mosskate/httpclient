package com.kp.pojo;

import java.util.Date;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Component
@Data	
@AllArgsConstructor
@NoArgsConstructor
public class MessageConvert {

   private   Integer  msgConvertPrimarykey;   
	
   private   Class<?>   convertType;
	   
   private   String   encodeTypeClient;
	  
   private   String   encodeTypeServer;
	  
   private   Date   MessageConvertTimes;

}
