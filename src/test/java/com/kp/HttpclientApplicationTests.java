package com.kp;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;

import javax.annotation.Resource;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.alibaba.fastjson.JSON;
import com.kp.pojo.ParamGetList;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes=HttpclientApplication.class)
@Slf4j
public class HttpclientApplicationTests {

	@Resource
	private   ParamGetList   paramGetList;
	  
	//x测试get
	@Test
	public void httpDoGet() {
			//xx获得Http客户端(可以理解为:你得先有一个浏览器;注意:实际上HttpClient与浏览器是不一样的)
			CloseableHttpClient httpClient = HttpClientBuilder.create().build();
			//xx创建Get请求
			HttpGet httpGet = new HttpGet("http://localhost:8090/server/init");
			//xx响应模型
			CloseableHttpResponse response = null;
			try {
				//xx由客户端执行(发送)Get请求
				response = httpClient.execute(httpGet);
				//xx从响应模型中获取响应实体
				HttpEntity responseEntity = response.getEntity();
				    log.info("响应状态为:" + response.getStatusLine());
				if (responseEntity != null) {
					log.info("响应内容长度为:" + responseEntity.getContentLength());
					//xx接口数据
					log.info("响应内容为:--->"+EntityUtils.toString(responseEntity));
				}
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					//x释放资源
					if (httpClient != null) {
						httpClient.close();
					}
					if (response != null) {
						response.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
     	}
    }
	   //x测试get有参数
	   @Test
	   public   void     getParamTest() {
	  //x获得Http客户端 
			CloseableHttpClient httpClient = HttpClientBuilder.create().build();
	        //x创建Get请求
			HttpGet httpGet = new HttpGet("http://localhost:8090/server/param?paramReset=123");
			//x响应模型
			CloseableHttpResponse response = null;
			try {
				//x配置信息
				RequestConfig requestConfig = RequestConfig.custom()
						//x设置连接超时时间(单位毫秒)
						.setConnectTimeout(5000)
						//x设置请求超时时间(单位毫秒)
						.setConnectionRequestTimeout(5000)
						//xsocket读写超时时间(单位毫秒)
						.setSocketTimeout(5000)
						//x设置是否允许重定向(默认为true)
						.setRedirectsEnabled(true).build();
				//x将上面的配置信息 运用到这个Get请求里
				httpGet.setConfig(requestConfig);
	 
				//x由客户端执行(发送)Get请求
					response = httpClient.execute(httpGet);
				//x从响应模型中获取响应实体
				HttpEntity responseEntity = response.getEntity();
				log.info("响应状态为:" + response.getStatusLine());
 				if (responseEntity != null) {
 					log.info("响应内容长度为:" + responseEntity.getContentLength());
 					log.info("响应内容为:" + EntityUtils.toString(responseEntity));
				}
				}
				catch(Exception  e){
					e.printStackTrace();
				}
			  finally {
				try {
					//x释放资源
					if (httpClient != null) {
						httpClient.close();
					}
					if (response != null) {
						response.close();
				  	}
			    	} 
				    catch (IOException e) {
					e.printStackTrace();
				    }
			        }
	 }
	     //post无参
	     @Test
		 public void doPostTestOne() {
			//x获得Http客户端(可以理解为:你得先有一个浏览器;注意:实际上HttpClient与浏览器是不一样的)
			CloseableHttpClient httpClient = HttpClientBuilder.create().build();
			//x创建Post请求
			HttpPost httpPost = new HttpPost("http://localhost:8090/server/postInit");
			//x响应模型
			CloseableHttpResponse response = null;
			try {
				//x由客户端执行(发送)Post请求
				response = httpClient.execute(httpPost);
				//x从响应模型中获取响应实体
				HttpEntity responseEntity = response.getEntity();
	 
				System.out.println("响应状态为:" + response.getStatusLine());
				if (responseEntity != null) {
					System.out.println("响应内容长度为:" + responseEntity.getContentLength());
					System.out.println("响应内容为:" + EntityUtils.toString(responseEntity));
				}
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					//x释放资源
					if (httpClient != null) {
						httpClient.close();
					}
					if (response != null) {
						response.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
       
        //post有参
	 	@Test
		public void doPostTestTwo() {
			//x获得Http客户端(可以理解为:你得先有一个浏览器;注意:实际上HttpClient与浏览器是不一样的)
			CloseableHttpClient httpClient = HttpClientBuilder.create().build();
			//x创建Post请求
			HttpPost httpPost = new HttpPost("http://localhost:8090/server/postParam");
			//x我这里利用阿里的fastjson，将Object转换为json字符串;
			paramGetList.setParamNumber(12321321);
			paramGetList.setParamReset("3123213");
			paramGetList.setRoutingCount(new BigDecimal("123123"));
			//x(需要导入com.alibaba.fastjson.JSON包)
			String jsonString = JSON.toJSONString(paramGetList);
			StringEntity entity = new StringEntity(jsonString, "UTF-8");
			//post请求是将参数放在请求体里面传过去的;这里将entity放入post请求体中
			httpPost.setEntity(entity);
			httpPost.setHeader("Content-Type", "application/json;charset=utf8");
			//x响应模型
			CloseableHttpResponse response = null;
			try {
				//x由客户端执行(发送)Post请求
				response = httpClient.execute(httpPost);
				//x从响应模型中获取响应实体
				HttpEntity responseEntity = response.getEntity();
				System.out.println("响应状态为:" + response.getStatusLine());
				if (responseEntity != null) {
					System.out.println("响应内容长度为:" + responseEntity.getContentLength());
					System.out.println("响应内容为:" + EntityUtils.toString(responseEntity));
				}
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					//x释放资源
					if (httpClient != null) {
						httpClient.close();
					}
					if (response != null) {
						response.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
 
}
